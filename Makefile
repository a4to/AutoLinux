# See LICENSE file for copyright and license details.

include config.mk

SRC = drw.c AutoLinux.c util.c
OBJ = ${SRC:.c=.o}

all: options AutoLinux

options:
	@echo AutoLinux build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

AutoLinux: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f AutoLinux ${OBJ} AutoLinux-${VERSION}.tar.gz *.orig *.rej

dist: clean
	mkdir -p AutoLinux-${VERSION}
	cp -R LICENSE Makefile config.mk\
		AutoLinux.1 drw.h util.h ${SRC} transient.c AutoLinux.desktop AutoLinux-${VERSION}
	tar -cf AutoLinux-${VERSION}.tar AutoLinux-${VERSION}
	gzip AutoLinux-${VERSION}.tar
	rm -rf AutoLinux-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f AutoLinux ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/AutoLinux
	mkdir -p ${DESTDIR}${PREFIX}/share/xsessions
	mkdir -p ${DESTDIR}${PREFIX}/share/licenses/autolinux
	cp -f LICENSE ${DESTDIR}${PREFIX}/share/licenses/autolinux
	cp -f AutoLinux.desktop ${DESTDIR}${PREFIX}/share/xsessions
	cp -arf icons ${DESTDIR}${PREFIX}/share/icons

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/AutoLinux\
		${DESTDIR}${PREFIX}/share/xsessions/AutoLinux.desktop\
		${DESTDIR}${PREFIX}/share/licenses/AutoLinux/LICENSE
	@for size in 1024x1024 128x128 16x16 192x192 22x22 24x24 256x256 32x32 36x36 48x48 512x512 64x64 72x72 96x96; do\
		rm -f ${DESTDIR}${PREFIX}/share/icons/hicolor/$${size}/apps/AutoLinux.png;\
	done

.PHONY: all options clean dist install uninstall
