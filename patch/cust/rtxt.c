
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    int pid = fork();
    if (pid == 0) {
        execl("/bin/sh", "sh", "-c", "rtxtBtn", NULL);
        exit(0);
    } else {
        int status;
        waitpid(pid, &status, 0);
    }

    return 0;
}
